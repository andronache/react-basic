import React from "react";

import { PostPrev } from "../";
import { Header } from "../";

const Home = (props) => {
  const posts = props.posts.reverse();
  return (
    <div className="container">
      <Header />
      <div className="col-md-9 wrapper">
        <div className="row">
          {
            posts.map((post, i) => {
              return (
                <PostPrev
                  key={post.id}
                  edit={props.edit}
                  del={props.del}
                  post={post} />
              );
            })
          }
        </div>
      </div>
    </div>
  );
}

export default Home;
