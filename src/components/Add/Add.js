import React from 'react';
import {
  withRouter
} from 'react-router-dom';

import { Header } from "../"

class Add extends React.Component {
  constructor() {
    super();
    this.state = {
      title: "Default title",
      content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean fermentum erat sed dolor placerat, pellentesque imperdiet lacus vehicula. Vestibulum scelerisque, massa nec sodales imperdiet, ligula est elementum metus, in tempor augue massa at leo. Morbi posuere fringilla ultrices. Ut convallis metus in urna tempus suscipit. Vestibulum non rhoncus lectus. Nunc semper vitae mauris eget semper. Vivamus iaculis ipsum semper odio blandit, non porta ipsum lobortis. Maecenas augue elit, tincidunt ut sapien a, laoreet consequat mauris. Suspendisse condimentum ut risus ut cursus. Aenean lacus felis, sagittis ac dui id, auctor condimentum eros. Donec et tempus massa. Pellentesque congue fringilla tincidunt."
    };
    this.onTitleChange = this.onTitleChange.bind(this);
    this.onContentChange = this.onContentChange.bind(this);
    this.callAdd = this.callAdd.bind(this);
  }
  onTitleChange(e) {
    this.setState({
      title: e.target.value
    });
  };
  onContentChange(e) {
    this.setState({
      content: e.target.value
    });
  };
  callAdd() {
    this.props.addNew(this.state.title, this.state.content);
    this.props.history.push("/");
  };
  render() {
    return (
      <div className="container">
        <Header />
        <div className="col-md-9 wrapper">
          <h4 className="marginTop20">Add new post</h4>
          <hr/>
          <form className="col-md-12">
            <div className="form-group margin-auto-pd30">
              <div className="row ">
                <label className="addLabel col-md-8">
                  <div>
                    <p>Title</p>
                  </div>
                  <input type="text" onChange={this.onTitleChange} className="form-control titleInput"/>
                </label>
              </div>
              <div className="row">
                <label className="addLabel">
                  <div>
                    <p>Content</p>
                  </div>
                  <textarea rows="10" onChange={this.onContentChange} className="form-control contentInput"></textarea>
                </label>
              </div>
              <button type="button" onClick={this.callAdd} className="btn btn-primary btn-md float-right">Submit</button>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

export default withRouter(Add);
