import React from "react";

import { Header } from "../";
import logo from "./react-logo.png";

const About = () => {
  return (
    <div className="container">
      <Header />
      <div className="col-md-9 wrapper aboutPage">
        <img style={{width: 70, marginBottom: 10}} alt={"logo"} src={logo} />
        <h5>Learn ReactJS @ Advanced Frameworks</h5>
        <hr />
        <h6>iQuest 2017</h6>
      </div>
    </div>
  );
}
export default About;
