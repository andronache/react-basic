import React from 'react';
import {
  withRouter
} from 'react-router-dom';

import { Header } from "../"

class Edit extends React.Component {
  constructor(props) {
    super();
    this.state = {
      title: props.location.state.post.title,
      content: props.location.state.post.content,
      id: props.location.state.post.id
    };
    this.onTitleChange = this.onTitleChange.bind(this);
    this.onContentChange = this.onContentChange.bind(this);
    this.callEdit = this.callEdit.bind(this);
  }
  onTitleChange(e) {
    this.setState({
      title: e.target.value
    });
  };
  onContentChange(e) {
    this.setState({
      content: e.target.value
    });
  };
  callEdit() {
    let info = {
      id: this.state.id,
      title: this.state.title,
      content: this.state.content
    }
    this.props.editPost(info);
    this.props.history.push("/");
  };
  render() {
    return (
      <div className="container">
        <Header />
        <div className="col-md-9 wrapper">
          <h4>Edit post</h4>
          <hr/>
          <form className="col-md-12">
            <div className="form-group margin-auto-pd30">
              <div className="row ">
                <label className="addLabel col-md-8">
                  <div>
                    <p>Title</p>
                  </div>
                  <input type="text"
                    onChange={(event) => this.onTitleChange(event)}
                    value={this.state.title}
                  className="form-control titleInput"/>
                </label>
              </div>
              <div className="row">
                <label className="addLabel">
                  <div>
                    <p>Content</p>
                  </div>
                  <textarea rows="10"
                    onChange={(event) => this.onContentChange(event)}
                    value={this.state.content}
                  className="form-control contentInput"></textarea>
                </label>
              </div>
              <button type="button" onClick={this.callEdit} className="btn btn-primary btn-md float-right">Submit</button>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

export default withRouter(Edit);
