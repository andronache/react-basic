export { default as Header } from "./Header/Header";
export { default as About } from "./About/About";
export { default as Add } from "./Add/Add";
export { default as Home } from "./Home/Home";
export { default as Post } from "./Post/Post";
export { default as PostPrev } from "./PostPrev/PostPrev";
export { default as Edit } from "./Edit/Edit";
