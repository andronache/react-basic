import React from "react";
import {
  Link,
  withRouter
} from 'react-router-dom';

const PostPrev = (props) => {
  const del = () => {
    props.del(props.post.id);
  }
  return (
    <div className="col-md-9 postPreview">
      <Link className="postTitle" to={{
        pathname: '/post/' + props.post.id,
        state: {
          post: props.post,
        }
      }}>{props.post.title}</Link>
      <span className="postDate">{props.post.date}</span>
      <hr/>
      <span>{props.post.content.substring(0, 249)}</span>
      <hr/>
      <button type="button" onClick={del} className="btn btn-danger btn-sm float-right editDeleteBtn">x</button>
      <Link className="postTitle" to={{
        pathname: '/edit',
        state: {
          post: props.post,
        }
      }}><button type="button" className="btn btn-outline-secondary btn-sm float-right editDeleteBtn">Edit</button></Link>
    </div>
  );
}

export default withRouter(PostPrev);
