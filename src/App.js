import React from 'react';
import { base } from './base';
import {
  BrowserRouter as Router,
  Route,
  Switch
} from 'react-router-dom';

import {
  Home,
  About,
  Add,
  Post,
  Edit
} from './components';

export default class App extends React.Component {
  constructor() {
    super();
    this.addPost = this.addPost.bind(this);
    this.deletePost = this.deletePost.bind(this);
    this.editPost = this.editPost.bind(this);
    this.state = {
      posts : []
    };
  };
  componentWillMount() {
    this.ref = base.syncState('posts', {
      context: this,
      state: 'posts',
      asArray: true
    });
  };
  componentWillUnmount() {
    base.removeBinding(this.ref);
  };
  addPost(title, content) {
    let posts = this.state.posts;
    let today = new Date();
    let dd = today.getDate();
    let mm = today.getMonth() + 1;
    let yyyy = today.getFullYear();
    let fullDate = dd + "." + mm + "." + yyyy;
    //random ID
    let postID = "";
    let pool = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for (let i = 0; i < 5; i++)
      postID += pool.charAt(Math.floor(Math.random() * pool.length));

    let post = {
      id: postID,
      title: title,
      content: content,
      date: fullDate
    };
    posts.push(post);
    this.setState({posts});
  }
  deletePost(post) {
    let posts = this.state.posts;
    for(let i = 0; i < posts.length; i++){
      if(posts[i].id === post){
        posts.splice(i, 1);
      }
    }
    this.setState({posts});
  }
  editPost(info) {
    let posts = this.state.posts;
    for(let i = 0; i < posts.length; i++){
      if(posts[i].id === info.id){
        posts[i].title = info.title;
        posts[i].content = info.content;
      }
    }
    this.setState({posts});
  }
  render() {
    return(
      <Router>
        <Switch>
          <Route exact path={"/"} render = {(props) => (
            <Home {...props} edit={this.editPost} del={this.deletePost} posts={this.state.posts}/>
          )} />
          <Route path={"/about"} component={About} />
          <Route path={"/post/:id"} render = {(props) => (
            <Post {...props} edit={this.editPost} del={this.deletePost} />
          )} />
          <Route path={"/edit"} render = {(props) => (
            <Edit {...props} editPost={this.editPost} />
          )} />
          <Route path={"/add"} render = {(props) => (
            <Add {...props} addNew={this.addPost} />
          )} />
        </Switch>
      </Router>
    );
  }
}
